import json
from pathlib import Path

import sc2
from bot.buildOrder import build_order
from bot.phoenix_open import phoenix

import random

from sc2 import run_game, maps, Race, Difficulty, game_info, position, Result
from sc2.player import Bot, Computer
from sc2.constants import *
from sc2.position import Point2, Point3
from sc2.unit import Unit
from sc2.ids.buff_id import BuffId
from sc2.ids.ability_id import AbilityId

class MyBot(sc2.BotAI):
	with open(Path(__file__).parent / "../botinfo.json") as f:
		NAME = json.load(f)["name"]

	def __init__(self):
		self.make_workers = True
		self.MAX_WORKERS = 50
		self.build_order = phoenix#build_order
		self.actions = []
		self.ITERATIONS_PER_MINUTE = 165
		self.scouts_and_spots = {}
		self.ling_rush = False
		self.make_units = True
		self.worker_rush = False
		self.proxy_rax = False
		self.opponent_race = "0"

	async def on_step(self, iteration):
		#self.iteration = iteration
		self.game_time = iteration / self.ITERATIONS_PER_MINUTE		#get real gametime

		if self.game_time == 0.30:									#introduce ourselves as gentlemen
			await self.chat_send(f"{self.NAME} here, let's GET WET.")
		if self.game_time <= 1.50:
			if self.enemy_race == Race.Random:							#determine the dirty randoms real identity
				drone = self.known_enemy_structures.filter(lambda unit: unit.type_id==HATCHERY)
				scv = self.known_enemy_structures.filter(lambda unit: unit.type_id==COMMANDCENTER)
				probe = self.known_enemy_structures.filter(lambda unit: unit.type_id==NEXUS)
				if probe:
					self.opponent_race = "Protoss"
				if drone:
					self.opponent_race = "Zerg"
				if scv:
					self.opponent_race = "Terran"
			if self.enemy_race == Race.Terran:
				self.opponent_race = "Terran"
			if self.enemy_race == Race.Protoss:
				self.opponent_race = "Protoss"
			if self.enemy_race == Race.Zerg:
				self.opponent_race = "Zerg"

		await self.distribute_workers()
		await build_order.early_blink(self)
		#await phoenix.phoenix_opener(self)
		await self.do_actions(self.actions)
		self.actions = []

	async def has_ability(self, ability, unit):
		abilities = await self.get_available_abilities(unit)
		if ability in abilities:
			return True
		else:
			return False

	def random_location_variance(self, location):
		x = location[0]
		y = location[1]

		x += random.randrange(-15,15)
		y += random.randrange(-15,15)

		if x < 0:
			print("x below")
			x = 0
		if y < 0:
			print("y below")
			y = 0
		if x > self.game_info.map_size[0]:
			print("x above")
			x = self.game_info.map_size[0]
		if y > self.game_info.map_size[1]:
			print("y above")
			y = self.game_info.map_size[1]

		go_to = position.Point2(position.Pointlike((x,y)))

		return go_to