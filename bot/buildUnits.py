import sc2
from sc2 import run_game, maps, Race, Difficulty, game_info, position, Result
from sc2.player import Bot, Computer
from sc2.constants import *
from sc2.position import Point2, Point3
from sc2.unit import Unit
from sc2.ids.buff_id import BuffId
from sc2.ids.ability_id import AbilityId

from pathlib import Path

class build_units:
	def __init__(self, unit):
		self.unit = unit

	async def build_workers(self):
		if len(self.units(NEXUS))*22 > len(self.units(PROBE)):
			if len(self.units(PROBE)) < self.MAX_WORKERS:
				for nexus in self.units(NEXUS).ready.idle:
					if self.can_afford(PROBE):
						self.actions.append(nexus.train(PROBE))

	async def build_zealot(self):
		if self.units(GATEWAY).ready.exists:
			if self.can_afford(ZEALOT):
				for gateway in self.units(GATEWAY).ready.idle:
						self.actions.append(gateway.train(ZEALOT))

	async def build_adept(self):
		if self.units(GATEWAY).ready.exists:
			if self.can_afford(ADEPT):
				for gateway in self.units(GATEWAY).ready.idle:
						self.actions.append(gateway.train(ADEPT))

	async def build_stalker(self):
		if self.units(GATEWAY).ready.exists:
			if self.can_afford(STALKER):
				for gateway in self.units(GATEWAY).ready.idle:
						self.actions.append(gateway.train(STALKER))

	async def warp_stalker(self):
		if self.make_units == True:
			for warpgate in self.units(WARPGATE).ready:
				abilities = await self.get_available_abilities(warpgate)
				if AbilityId.WARPGATETRAIN_STALKER in abilities:
					pylon = self.units(PYLON).ready.random
					pos = pylon.position.to2.random_on_distance(6)
					placement = await self.find_placement(AbilityId.WARPGATETRAIN_STALKER, pos, placement_step=1)
					if placement is None:
						print("Can't Place")
						return
					self.actions.append(warpgate.warp_in(STALKER, placement))

	async def warp_zealot(self):
		if self.make_units == True:
			for warpgate in self.units(WARPGATE).ready:
				abilities = await self.get_available_abilities(warpgate)
				if AbilityId.WARPGATETRAIN_STALKER in abilities:
					pylon = self.units(PYLON).ready.random
					pos = pylon.position.to2.random_on_distance(6)
					placement = await self.find_placement(AbilityId.WARPGATETRAIN_ZEALOT, pos, placement_step=1)
					if placement is None:
						print("Can't Place")
						return
					self.actions.append(warpgate.warp_in(ZEALOT, placement))

	async def warp_adept(self):
		for warpgate in self.units(WARPGATE).ready:
			abilities = await self.get_available_abilities(warpgate)
			if AbilityId.TRAINWARP_ADEPT in abilities:
				pylon = self.units(PYLON).ready.random
				pos = pylon.position.to2.random_on_distance(6)
				placement = await self.find_placement(AbilityId.TRAINWARP_ADEPT, pos, placement_step=1)
				if placement is None:
					print("Can't Place")
					return
				self.actions.append(warpgate.warp_in(ADEPT, placement))

	async def build_obs(self):
		if self.units(ROBOTICSFACILITY).ready.exists:
			if self.can_afford(OBSERVER):
				for robo in self.units(ROBOTICSFACILITY).ready.idle:
					self.actions.append(robo.train(OBSERVER))

	async def build_phoenix(self):
		if self.units(STARGATE).ready.exists:
			if self.can_afford(PHOENIX):
				for star in self.units(STARGATE).ready.idle:
					self.actions.append(star.train(PHOENIX))