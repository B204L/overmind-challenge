import sc2
from sc2 import run_game, maps, Race, Difficulty, game_info, position, Result
from sc2.player import Bot, Computer
from sc2.constants import *
from sc2.position import Point2, Point3
from sc2.unit import Unit
from sc2.ids.buff_id import BuffId
from sc2.ids.ability_id import AbilityId
from pathlib import Path

class research_upgrades:
	def __init__(self, unit):
		self.unit = unit

	async def research_warpgate(self):
		if self.units(CYBERNETICSCORE).ready.exists:
			cybercore = self.units(CYBERNETICSCORE).first
			if cybercore.noqueue and self.has_ability(RESEARCH_WARPGATE, cybercore):
				if self.can_afford(RESEARCH_WARPGATE) and not self.units(WARPGATE).exists:
					self.actions.append(cybercore(RESEARCH_WARPGATE))

	async def research_blink(self):
		if self.units(TWILIGHTCOUNCIL).ready.exists:
			twilight = self.units(TWILIGHTCOUNCIL).first
			if twilight.noqueue and self.has_ability(RESEARCH_BLINK, twilight):
				if self.can_afford(RESEARCH_BLINK):
					self.actions.append(twilight(RESEARCH_BLINK))