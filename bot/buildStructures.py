import sc2
from sc2 import run_game, maps, Race, Difficulty, game_info, position, Result
from sc2.player import Bot, Computer
from sc2.constants import *
from sc2.position import Point2, Point3
from sc2.unit import Unit
from sc2.ids.buff_id import BuffId
from sc2.ids.ability_id import AbilityId
#from sc2.game_info import Ramp

from pathlib import Path
import math
import random

class build_structures():
	def __init__(self, unit):
		self.unit = unit

	async def build_expo(self):
		if self.can_afford(NEXUS) and not self.already_pending(NEXUS):
			await self.expand.now()

	async def supply_pylon(self):
		if self.supply_left < 7 and not self.already_pending(PYLON):
				nexuses = self.units(NEXUS).random
				max_difference = math.pi
				pos = nexuses.position.towards_with_random_angle(self.game_info.map_center, random.randrange(2,10))
				if self.units(NEXUS).exists:
					if self.can_afford(PYLON):
						await self.build(PYLON, near=pos)

	async def build_gateway(self):
		if self.units(PYLON).ready.exists:
			pos = self.units(PYLON).ready.random
			if self.can_afford(GATEWAY) and self.already_pending(GATEWAY) <= 1:
				await self.build(GATEWAY, near=pos)

	async def build_gas(self):
		for nexus in self.units(NEXUS).ready:
			empty_geysers = self.state.vespene_geyser.closer_than(10.0, nexus)
			for empty_geyser in empty_geysers:
				if not self.can_afford(ASSIMILATOR):
					break
				worker = self.select_build_worker(empty_geyser.position)
				if worker is None:
					break
				if not self.units(ASSIMILATOR).closer_than(1.0, empty_geyser).exists and self.units(GATEWAY).exists and self.supply_left <= 16:
					self.actions.append(worker.build(ASSIMILATOR, empty_geyser))

	async def build_cybercore(self):
		if self.units(PYLON).ready.exists:
			pos = self.units(PYLON).ready.random
			if self.units(GATEWAY).ready.exists:
				if not self.units(CYBERNETICSCORE) and self.can_afford(CYBERNETICSCORE) and not self.already_pending(CYBERNETICSCORE):
					await self.build(CYBERNETICSCORE, near=pos)

	async def build_twilight(self):
		if self.units(CYBERNETICSCORE).ready.exists:
			pos = self.units(PYLON).ready.random
			if not self.units(TWILIGHTCOUNCIL) and self.can_afford(TWILIGHTCOUNCIL) and not self.already_pending(TWILIGHTCOUNCIL):
				await self.build(TWILIGHTCOUNCIL, near=pos)

	async def build_battery(self):
		if self.units(PYLON).ready.exists:
			nexuses = self.units(NEXUS).first
			#pos = nexuses.position.towards_with_random_angle(self.game_info.map_center, random.randrange(8,10))
			pos = nexuses.position.towards(self.main_base_ramp.barracks_correct_placement, random.randrange (12,13))
			if not self.already_pending(SHIELDBATTERY) and self.can_afford(SHIELDBATTERY):
				await self.build(SHIELDBATTERY, near=pos)

	async def build_robo(self):
		if self.units(CYBERNETICSCORE).ready.exists:
			nexuses = self.units(NEXUS).first
			pos = nexuses.position.towards_with_random_angle(self.game_info.map_center, random.randrange(4, 10))
			if not self.already_pending(ROBOTICSFACILITY) and self.can_afford(ROBOTICSFACILITY):
				await self.build(ROBOTICSFACILITY, near=pos)

	async def build_stargate(self):
		if self.units(CYBERNETICSCORE).ready.exists:
			nexuses = self.units(NEXUS).random
			pos = nexuses.position.towards_with_random_angle(self.game_info.map_center, random.randrange(4, 6))
			if self.can_afford(STARGATE):
				await self.build(STARGATE, near=pos)

	async def ramp_wall(self):
		nexuses = self.units(NEXUS).first
		pos = nexuses.position.towards(self.main_base_ramp.barracks_correct_placement, random.randrange (11, 12))
		pylons = self.units(PYLON).amount
		gateways = self.units(GATEWAY)
		if pylons < 1 and self.can_afford(PYLON):
			await self.build(PYLON, near=pos)
		if self.units(PYLON).ready and not gateways:
			await self.build(GATEWAY, near=self.main_base_ramp.barracks_correct_placement)
		if self.units(GATEWAY).ready and not self.units(CYBERNETICSCORE):
			placement_position = self.main_base_ramp.corner_depots
			target_position = placement_position.pop()
			if not self.already_pending(CYBERNETICSCORE):
				await self.build(CYBERNETICSCORE, near=target_position)