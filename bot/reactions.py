import sc2
from sc2 import run_game, maps, Race, Difficulty, game_info, position, Result
from sc2.player import Bot, Computer
from sc2.constants import *
from sc2.position import Point2, Point3
from sc2.unit import Unit
from sc2.ids.buff_id import BuffId
from sc2.ids.ability_id import AbilityId

from bot.buildStructures import build_structures
from bot.buildUnits import build_units
from bot.researchUpgrades import research_upgrades
from bot.unitMicro import micro

class react:
	def __init__(self):
		self.unit = unit

	async def early_pool(self):
		pool = self.known_enemy_structures.filter(lambda unit: unit.type_id==SPAWNINGPOOL)
		if pool:
			self.ling_rush = True

		if self.ling_rush == True:
			if self.units(SHIELDBATTERY).amount == 0 and not self.already_pending(SHIELDBATTERY):
				if self.units(CYBERNETICSCORE).ready:
					await build_structures.build_battery(self)

			if self.units(GATEWAY).ready:
				gates = self.units(GATEWAY).ready
				abilities = await self.get_available_abilities(gates)
				if AbilityId.MORPH_WARPGATE in abilities:
					await micro.morph_gate(self)
				else:
					await build_units.build_zealot(self)
					await micro.boost_gate(self)

			if self.units(CYBERNETICSCORE).ready and self.units(WARPGATE).ready:
				await build_units.warp_adept(self)

			if self.units(ZEALOT).amount + self.units(ADEPT).amount >= 6:
				self.ling_rush = False

			await micro.choke_point(self)
			await micro.worker_save(self)

	async def early_rax(self):
		if self.opponent_race == "Terran":
			cc = self.known_enemy_structures.filter(lambda unit: unit.type_id==COMMANDCENTER)
			rax_count = self.known_enemy_structures.filter(lambda unit: unit.type_id==BARRACKS).amount
			rax = self.known_enemy_structures.filter(lambda unit: unit.type_id==BARRACKS)
			#rax_in_main = rax.filter(lambda structure: any(structure.distance_to(cc) <= 15 for barracks in self.known_enemy_structures(BARRACKS)))
			if rax_count >= 2:
				self.proxy_rax = True
				print("Proxy 1")
			if cc and not rax:
				if self.game_time <= 1.50:
					self.proxy_rax = True
					print("Proxy 2")

		if self.proxy_rax == True and self.units(SHIELDBATTERY).amount < 2:
			if self.units(CYBERNETICSCORE).ready:
				await build_structures.build_battery(self)

		if self.units(GATEWAY).ready and self.units(CYBERNETICSCORE).ready:
			if self.units(STALKER).amount < 10:
				gates = self.units(GATEWAY).ready
				abilities = await self.get_available_abilities(gates)
				if AbilityId.MORPH_WARPGATE in abilities:
					await micro.morph_gate(self)
				else:
					await build_units.build_stalker(self)
					await micro.boost_gate(self)

		if self.proxy_rax == True:

			if self.units(GATEWAY).amount <= 2:
				await build_structures.build_gateway(self)

			#if self.units(GATEWAY).ready and self.units(CYBERNETICSCORE).ready:
			#	await build_units.build_stalker(self)
			#	await micro.boost_gate(self)

			#if self.units(STALKER).amount < 10 and self.units(GATEWAY).ready:
			#	if not self.units(WARPGATE):
			#		await build_units.build_stalker(self)
			#		await micro.boost_gate(self)

			#if self.units(STALKER).amount < 10 and self.units(WARPGATE).ready:
			#	await build_units.warp_stalker(self)
			if self.units(GATEWAY).ready and self.units(CYBERNETICSCORE).ready:
				gates = self.units(GATEWAY).ready
				abilities = await self.get_available_abilities(gates)
				if AbilityId.MORPH_WARPGATE in abilities:
					await micro.morph_gate(self)
				elif AbilityId.MORPH_WARPGATE not in abilities:
					await build_units.build_stalker(self)
					await micro.boost_gate(self)

			if self.units(WARPGATE).ready:
				await build_units.warp_stalker(self)

			if self.units(STALKER).amount >= 10:
				self.proxy_rax = False