import sc2
from sc2 import run_game, maps, Race, Difficulty, game_info, position, Result
from sc2.player import Bot, Computer
from sc2.constants import *
from sc2.position import Point2, Point3
from sc2.unit import Unit
from sc2.ids.buff_id import BuffId
from sc2.ids.ability_id import AbilityId

from bot.buildStructures import build_structures
from bot.buildUnits import build_units
from bot.researchUpgrades import research_upgrades
from bot.unitMicro import micro
from bot.reactions import react

class build_order:
	def __init__(self):
		self.unit = unit

	async def early_blink(self):

		if self.game_time < 2.00:
			await micro.worker_rush(self)

		if self.game_time < 1.50:
			await micro.scout(self)
			await react.early_pool(self)
			await react.early_rax(self)
		if self.ling_rush == True:
			await react.early_pool(self)
		if self.proxy_rax == True:
			await react.early_rax(self)

		if self.worker_rush == True:
			if self.units(PYLON).ready and self.units(GATEWAY).amount == 0:
				await build_structures.build_gateway(self)
				self.worker_rush = False

		if self.make_workers == True:
			await build_units.build_workers(self)

		if self.game_time > 1.50:
			await micro.defend_base(self)

		if self.game_time < 2.00:
			await micro.boost_probes(self)

		if self.game_time < 2.00:
			await build_structures.ramp_wall(self)

		#if self.supply_used >= 14:
		#	self.make_workers = False
		#	await build_structures.supply_pylon(self)
		#	self.make_workers = True

		#if self.units(PYLON).ready and self.game_time < 1.50:
		#	self.make_workers = False
		#	await build_structures.build_gateway(self)
		#	self.make_workers = True

		if self.already_pending(GATEWAY) and self.game_time <= 1.50:
			self.make_workers = False
			await build_structures.build_gas(self)
			self.make_workers = True

		if self.units(ADEPT).amount < 2 and self.units(CYBERNETICSCORE).ready:
			if not self.ling_rush:
				await build_units.build_adept(self)

		if self.units(NEXUS).amount == 2:
			await build_structures.build_gas(self)

		if self.supply_used == 19 and self.game_time <= 1.50:
			await build_structures.build_gateway(self)

		#if self.units(GATEWAY).ready:
		#	self.make_workers = False
		#	await build_structures.build_cybercore(self)
		#	self.make_workers = True

		if self.supply_used >= 19 and self.game_time <= 2.00:
			self.make_workers = False
			await build_structures.supply_pylon(self)
			self.make_workers = True

		if self.units(CYBERNETICSCORE).ready and not self.units(WARPGATE).exists:
			await research_upgrades.research_warpgate(self)
			if not self.ling_rush and not self.proxy_rax:
				await micro.boost_warpgate(self)

		if self.game_time > 2.50:
			await build_structures.supply_pylon(self)
		
		if self.units(CYBERNETICSCORE).ready and not self.ling_rush and not self.proxy_rax:
			await build_structures.build_twilight(self)
		
		if self.units(STALKER).amount < 2 and self.ling_rush == False:
			await build_units.warp_stalker(self)

		if self.game_time > 2.80 and not self.ling_rush and not self.proxy_rax: #maybe fix
			if self.units(NEXUS).amount < 2 and not self.already_pending(NEXUS):
				if self.can_afford(NEXUS):
					await self.expand_now()

		if self.units(TWILIGHTCOUNCIL).ready:
			twilight = self.units(TWILIGHTCOUNCIL).ready.first
			blink = await self.get_available_abilities(twilight)
			if AbilityId.RESEARCH_BLINK in blink:
				await research_upgrades.research_blink(self)
			if not twilight.has_buff(BuffId.CHRONOBOOSTENERGYCOST):
				await micro.boost_blink(self)

		if self.game_time > 3.50:
			if self.units(WARPGATE).amount < 6 and self.units(NEXUS).amount == 2:
				await build_structures.build_gateway(self)

		if self.game_time > 3.60:
			await build_structures.supply_pylon(self)

		if self.game_time > 4.10:
			if self.units(STALKER).amount < 6 and self.units(NEXUS).amount == 2:
				await build_units.warp_stalker(self)

		if self.game_time > 4.20 and self.supply_left <= 10:
			await build_structures.supply_pylon(self)

		if self.game_time > 4.50 and self.units(NEXUS).amount == 2:
			if self.units(STALKER).amount < 20:
				await build_units.warp_stalker(self)
			if self.units(STALKER).amount > 12:
				await micro.f2_amove(self)
			else:
				await micro.defend_base(self)
			await micro.blink_micro(self)

		if self.game_time > 7.00 and self.units(NEXUS).amount == 2:
			await self.expand_now()

		if self.units(NEXUS).amount == 3:
			await build_structures.build_gas(self)

		if self.game_time > 8.00 and not self.units(ROBOTICSFACILITY).exists:
			if self.units(CYBERNETICSCORE).ready:
				await build_structures.build_robo(self)

		if self.units(ROBOTICSFACILITY).ready:
			if self.units(OBSERVER).amount <= 10:
				await build_units.build_obs(self)

		if self.units(OBSERVER).idle:
			for scout in self.units(OBSERVER).idle:
				await micro.scout(self)

		if self.game_time > 8.50 and self.units(NEXUS).amount >= 3:
				if self.units(STALKER).amount < 60:
					await build_units.warp_stalker(self)
				if self.units(STALKER).amount > 12:
					await micro.f2_amove(self)
				else:
					await micro.defend_base(self)
				await micro.blink_micro(self)

		if self.units(STALKER).amount >= 60:
			await self.expand_now()

