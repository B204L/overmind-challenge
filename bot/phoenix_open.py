import sc2
from sc2 import run_game, maps, Race, Difficulty, game_info, position, Result
from sc2.player import Bot, Computer
from sc2.constants import *
from sc2.position import Point2, Point3
from sc2.unit import Unit
from sc2.ids.buff_id import BuffId
from sc2.ids.ability_id import AbilityId

from bot.buildStructures import build_structures
from bot.buildUnits import build_units
from bot.researchUpgrades import research_upgrades
from bot.unitMicro import micro
from bot.reactions import react

class phoenix:
	def __init__(self):
		self.unit = unit

	async def phoenix_opener(self):
########## early game bs detection ###################
		if self.game_time < 2.00:
			await micro.worker_rush(self)

		if self.game_time < 1.50:
			await micro.scout(self)
			await react.early_pool(self)
			await react.early_rax(self)
		if self.ling_rush == True:
			await react.early_pool(self)
		if self.proxy_rax == True:
			await react.early_rax(self)

		if self.worker_rush == True:
			if self.units(PYLON).ready and self.units(GATEWAY).amount == 0:
				await build_structures.build_gateway(self)
				self.worker_rush = False

		if self.make_workers == True:
			await build_units.build_workers(self)
		if self.units(PROBE).amount == 13:
			self.make_workers = False
		if self.units(PYLON).amount >= 1:
			self.make_workers = True

		if self.game_time > 1.50:
			await micro.defend_base(self)

		if self.game_time < 2.00:
			await micro.boost_probes(self)

		if self.game_time < 2.00:
			await build_structures.ramp_wall(self)

		if self.game_time <= 2.50:
			if self.supply_left <= 4:
				await build_structures.supply_pylon(self)

		if self.game_time >= 5.00:
			if self.supply_left <= 12:
				await build_structures.supply_pylon(self)

		if self.game_time >= 8.00:
			if self.supply_left <= 16:
				await build_structures.supply_pylon(self)

		if self.supply_left == 0: #a robot shouldnt get supply blocked..
			await build_structures.supply_pylon(self)

######################################################

		if self.units(NEXUS).amount == 1 and self.units(CYBERNETICSCORE).exists:
			self.make_workers = False
			await self.expand_now()
			if self.already_pending(NEXUS):
				self.make_workers = True

		if self.already_pending(GATEWAY) and self.game_time <= 1.10:
			self.make_workers = False
			await build_structures.build_gas(self)
			self.make_workers = True

		if self.units(GATEWAY).amount == 1 and self.units(NEXUS).amount == 2:
			await build_structures.build_gateway(self)

		if self.units(ADEPT).amount < 2 and self.units(CYBERNETICSCORE).ready:
			if not self.ling_rush:
				await build_units.build_adept(self)

		if self.units(CYBERNETICSCORE).ready and not self.units(WARPGATE).exists:
			await research_upgrades.research_warpgate(self)
			if not self.ling_rush and not self.proxy_rax:
				await micro.boost_warpgate(self)
############## PHOENIX TIME ######################
		if self.units(CYBERNETICSCORE).ready and self.units(NEXUS).amount == 2:
			if self.units(STARGATE).amount <= 1:
				await build_structures.build_stargate(self)

		if self.units(STARGATE).ready.noqueue:
			if self.units(PHOENIX).amount <= 6:
				await build_units.build_phoenix(self)
			else:
				phoenix = self.units(PHOENIX)
				for p in phoenix:
					self.actions.append(p.move(self.enemy_start_locations[0]))
					await micro.phoenix_harass(self)

		if not self.units(STARGATE).noqueue:
			await micro.boost_stargate(self)
