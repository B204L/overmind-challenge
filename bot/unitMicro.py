import sc2
from sc2 import run_game, maps, Race, Difficulty, game_info, position, Result
from sc2.player import Bot, Computer
from sc2.constants import *
from sc2.position import Point2, Point3
from sc2.unit import Unit
from sc2.ids.buff_id import BuffId
from sc2.ids.ability_id import AbilityId

from pathlib import Path

class micro():
	def __init__(self, unit):
		self.unit = unit

	#async def kite_enemy(self):
	#	army = self.units(STALKER)
	#	enemies = self.known_enemy_units
	#	near_enemies = enemies.filter(lambda enemy: any(enemy.distance_to(army) < 6 for army in self.units(STALKER)))
	#	for forces in army:
	#		low_hp = int(army, lambda u: u.health + u.shield)
	#		pos = self.units(NEXUS).first
	#		if near_enemies and low_hp <= 20:
	#			self.actions.append(forces.move(further_than(4, pos)))
	#		else:
	#			return

	async def choke_point(self):
		zealots = self.units(ZEALOT)
		adepts = self.units(ADEPT)
		stalker = self.units(STALKER)
		melee_enemies = self.known_enemy_units.of_type(ZERGLING)
		lings_in_base = melee_enemies.filter(lambda enemy: any(enemy.distance_to(nexus) < 25 for nexus in self.units(NEXUS)))
		buildings = self.units(PYLON) | self.units(NEXUS) | self.units(GATEWAY) | self.units(CYBERNETICSCORE) | self.units(SHIELDBATTERY)
		for zealot in zealots:
			if lings_in_base:
				max_hp = 160
				damaged_zealot = zealot.health + zealot.shield#min(zealot, key=lambda e: e.health + e.shield)
				pos = buildings.closest_distance_to(zealot)
				print(pos)
				if damaged_zealot < 140 and pos >= 2:
					#pos = buildings.closest_to(damaged_zealot)
					self.actions.append(zealot.move(buildings.closest_to(zealot)))
				else:
					closest_enemy = self.known_enemy_units.closest_to(zealot)
					self.actions.append(zealot.attack(closest_enemy))

	async def worker_rush(self):
		defending_army = self.units(PROBE)
		enemy_workers = self.known_enemy_units.of_type(PROBE) | self.known_enemy_units.of_type(SCV) | self.known_enemy_units.of_type(DRONE)
		near_enemies = enemy_workers.filter(lambda enemy: any(enemy.distance_to(nexus) < 20 for nexus in self.units(NEXUS)))
		amount_enemies = enemy_workers.amount
		for forces in defending_army:
			if near_enemies and amount_enemies > 1:
				self.worker_rush = True
				in_range_enemies = near_enemies.in_attack_range_of(forces)
				if in_range_enemies:
					lowest_hp = min(in_range_enemies, key=lambda e: e.health + e.shield)
					self.actions.append(forces.attack(lowest_hp))
				else:
					self.actions.append(forces.move(near_enemies.closest_to(forces)))
			else:
				self.distribute_workers()
			attacking_minerals = enemy_workers.filter(lambda enemy: any(enemy.distance_to(probe) < 1 for probe in self.units(PROBE)))
			if attacking_minerals:
				in_range_enemies = near_enemies.in_attack_range_of(forces)
				if in_range_enemies:
					lowest_hp = min(in_range_enemies, key=lambda e: e.health + e.shield)
					self.actions.append(forces.attack(lowest_hp))
			else:
				self.distribute_workers()

	async def morph_gate(self):
		gates = self.units(GATEWAY).ready
		abilities = await self.get_available_abilities(gates)
		if AbilityId.MORPH_WARPGATE in abilities:
			self.actions.append(gates(AbilityId.MORPH_WARPGATE, gates))

	async def worker_save(self):
		army = (self.units(STALKER) | self.units(ADEPT) | self.units(ZEALOT))
		probes = self.units(PROBE)
		lings = self.known_enemy_units.of_type(ZERGLING)
		near_enemies = lings.filter(lambda enemy: any(enemy.distance_to(nexus) < 5 for nexus in self.units(NEXUS).ready))
		attacking_minerals = lings.filter(lambda enemy: any(enemy.distance_to(probe) < 1 for probe in self.units(PROBE)))
		if attacking_minerals and near_enemies:
			for workers in probes:
				lowest_hp = min(attacking_minerals, key=lambda e: e.health + e.shield)
				self.actions.append(workers.attack(lowest_hp))

	async def f2_amove(self):
		attacking_army = (self.units(STALKER) | self.units(ADEPT) | self.units(ZEALOT))
		for offense in attacking_army:
			if not self.known_enemy_units:
				self.actions.append(offense.attack(self.enemy_start_locations[0]))
			else:
				nexuses = self.units(NEXUS).ready.random
				closest_enemy = self.known_enemy_units.closest_to(offense)
				self.actions.append(offense.attack(closest_enemy))

	async def blink_micro(self):
		stalker = (self.units(STALKER))
		enemies = self.known_enemy_units
		#near_enemies = enemies.filter(lambda enemy: any(enemy.distance_to(stalker) < 6 for stalker in self.units(STALKER)))
		#near_enemies = enemy_workers.filter(lambda enemy: any(enemy.distance_to(nexus) < 20 for nexus in self.units(NEXUS)))
		for forces in stalker:
			near_enemies = enemies.filter(lambda enemy: any(enemy.distance_to(stalker) < 5 for stalker in self.units(STALKER)))
			in_range_enemies = near_enemies.in_attack_range_of(forces)
			escape_location = forces.position.towards(self.units(NEXUS).first, 6)
			#lowest_hp = min(in_range_enemies, key=lambda e: e.health + e.shield)
			#move_pos = forces.position.away(enemies)
			if forces.shield < 4:
				self.actions.append(forces(EFFECT_BLINK_STALKER, escape_location))
			if near_enemies:
				self.actions.append(forces.move(escape_location))
			if in_range_enemies:
					lowest_hp = min(in_range_enemies, key=lambda e: e.health + e.shield)
					self.actions.append(forces.attack(lowest_hp))



	async def unit_rally(self):
		moving_army = (self.units(STALKER))
		for moving in moving_army:
			self.actions.append(moving.move(self.game_info.map_center))

	async def defend_base(self):
		defending_army = self.units(STALKER) | self.units(ADEPT) | self.units(ZEALOT)
		near_enemies = self.known_enemy_units.filter(lambda enemy: any(enemy.distance_to(nexus) < 20 for nexus in self.units(NEXUS)))
		for forces in defending_army:
			if near_enemies:
				in_range_enemies = near_enemies.in_attack_range_of(forces)
				if in_range_enemies:
					lowest_hp = min(in_range_enemies, key=lambda e: e.health + e.shield)
					self.actions.append(forces.attack(lowest_hp))
				else:
					self.actions.append(forces.move(near_enemies.closest_to(forces)))

	async def boost_probes(self):
		nexus = self.units(NEXUS).ready.random
		if not nexus.has_buff(BuffId.CHRONOBOOSTENERGYCOST):
			abilities = await self.get_available_abilities(nexus)
			if AbilityId.EFFECT_CHRONOBOOSTENERGYCOST in abilities:
				self.actions.append(nexus(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST, nexus))

	async def boost_warpgate(self):
		if self.units(CYBERNETICSCORE).exists and self.units(CYBERNETICSCORE).ready:
			nexus = self.units(NEXUS).ready.random
			ccore = self.units(CYBERNETICSCORE).first
			if not ccore.has_buff(BuffId.CHRONOBOOSTENERGYCOST) and self.has_ability(RESEARCH_WARPGATE, ccore):
				abilities = await self.get_available_abilities(nexus)
				if AbilityId.EFFECT_CHRONOBOOSTENERGYCOST in abilities:
					self.actions.append(nexus(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST, ccore))

	async def boost_gate(self):
		if self.units(GATEWAY).exists and self.units(GATEWAY).ready:
			nexus = self.units(NEXUS).ready.random
			gates = self.units(GATEWAY).ready.random
			if not gates.has_buff(BuffId.CHRONOBOOSTENERGYCOST):
				abilities = await self.get_available_abilities(nexus)
				if AbilityId.EFFECT_CHRONOBOOSTENERGYCOST in abilities:
					self.actions.append(nexus(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST, gates))

	async def boost_stargate(self):
		if self.units(STARGATE).exists and not self.units(STARGATE).idle:
			nexus = self.units(NEXUS).ready.random
			star = self.units(STARGATE).ready.random
			if not star.has_buff(BuffId.CHRONOBOOSTENERGYCOST):
				abilities = await self.get_available_abilities(nexus)
				if AbilityId.EFFECT_CHRONOBOOSTENERGYCOST in abilities:
					self.actions.append(nexus(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST, star))

	async def boost_blink(self):
		if self.units(TWILIGHTCOUNCIL).exists and self.units(TWILIGHTCOUNCIL).ready:
			nexus = self.units(NEXUS).ready.random
			council = self.units(TWILIGHTCOUNCIL).first
			if not council.has_buff(BuffId.CHRONOBOOSTENERGYCOST) and self.has_ability(RESEARCH_BLINK, council):
				abilities = await self.get_available_abilities(nexus)
				if AbilityId.EFFECT_CHRONOBOOSTENERGYCOST in abilities:
					self.actions.append(nexus(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST, council))

	async def phoenix_harass(self):
		phoenix = self.units(PHOENIX)
		enemy_workers = self.known_enemy_units.of_type(PROBE) | self.known_enemy_units.of_type(SCV) | self.known_enemy_units.of_type(DRONE)
		graviton_energy = phoenix.filter(lambda e: any(e.energy >= 50 for beam in phoenix))
		near_workers = phoenix.filter(lambda beam: any(beam.distance_to(worker) <= 5 for worker in enemy_workers))
		#lifted_workers = enemy_workers.filter(lambda unit: any (unit.is_flying))
		for p in phoenix:
			if near_workers and graviton_energy:
				#self.actions.append(p(AbilityId.GRAVITONBEAM_GRAVITONBEAM, enemy_workers.random))
				self.actions.append(p(AbilityId.GRAVITONBEAM_GRAVITONBEAM, enemy_workers.first))
				#self.actions.append(p(AbilityId.GRAVITONBEAM_GRAVITONBEAM, enemy_workers.closest_to(p)))
		for worker in enemy_workers.flying:
			print(worker)
			#if worker.is_flying:
			#	print("enemy_workers.is_flying")

	async def scout(self):
		self.expand_dis_dir = {}
		for el in self.expansion_locations:
			distance_to_enemy_start = el.distance_to(self.enemy_start_locations[0])
			self.expand_dis_dir[distance_to_enemy_start] = el

		self.ordered_exp_distances = sorted(k for k in self.expand_dis_dir)

		existing_ids = [unit.tag for unit in self.units]
		to_be_removed = []
		for noted_scout in self.scouts_and_spots:
			if noted_scout not in existing_ids:
				to_be_removed.append(noted_scout)

		for scout in to_be_removed:
			del self.scouts_and_spots[scout]

		if len(self.units(ROBOTICSFACILITY).ready) == 0:
			unit_type = PROBE
			unit_limit = 1
		else:
			unit_type = OBSERVER
			unit_limit = 3

		assign_scout = True

		if unit_type == PROBE:
			for unit in self.units(PROBE):
				if unit.tag in self.scouts_and_spots:
					assign_scout = False

		if assign_scout:
			if len(self.units(unit_type).idle) > 0:
				for obs in self.units(unit_type).idle[:unit_limit]:
					if obs.tag not in self.scouts_and_spots:
						for dist in self.ordered_exp_distances:
							try:
								location = self.expand_dis_dir[dist] #next(value for key, value in self.expand_dis_dir.items() if key == dist)
								active_locations = [self.scouts_and_spots[k] for k in self.scouts_and_spots]

								if location not in active_locations:
									if unit_type == PROBE:
										for unit in self.units(PROBE):
											if unit.tag in self.scouts_and_spots:
												continue

									self.actions.append(obs.move(location))
									self.scouts_and_spots[obs.tag] = location
									break
							except Exception as e:
								pass

		for obs in self.units(unit_type):
			if obs.tag in self.scouts_and_spots:
				if obs in [probe for probe in self.units(PROBE)] and self.game_time <= 2.2:
					self.actions.append(obs.move(self.random_location_variance(self.scouts_and_spots[obs.tag])))

